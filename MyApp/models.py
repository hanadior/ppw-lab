from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class ActivitySchedule(models.Model):
	activity = models.CharField(max_length = 100)
	category = models.CharField(max_length = 100)
	place = models.CharField(max_length = 100)
	day = models.CharField(max_length = 100)
	date = models.DateField()
	hour = models.TimeField()
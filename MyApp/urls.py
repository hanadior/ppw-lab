from django.conf.urls import url
from .views import index
from .views import about
from .views import contact
from .views import kitchen
from .views import signup
from .views import schedule
from .views import schedule_result
from .views import schedule_delete
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^about/', about, name='about'),
    url(r'^contact/', contact, name='contact'),
    url(r'^kitchen/', kitchen, name='kitchen'),
    url(r'^signup/', signup, name='signup'),
	url(r'^schedule/', schedule, name='schedule'),
	url(r'^schedule_result/', schedule_result, name='schedule_result'),
	url(r'^schedule_delete/', schedule_delete, name='schedule_delete'),]
	
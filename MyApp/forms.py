from django import forms

class Message_Form(forms.Form):
	error_messages = {
		'required' : 'Please fill this blank',
		}
	attrs = {
		'size': 100,
		'class': 'form-control'
	}
	activity = forms.CharField(label='Activity', required=True, widget=forms.TextInput(attrs=attrs))
	category = forms.CharField(label='Category', required=True, widget=forms.TextInput(attrs=attrs))
	place = forms.CharField(label='Place', required=True, widget=forms.TextInput(attrs=attrs))
	day = forms.CharField(label='Day', required=True, widget=forms.TextInput(attrs=attrs))
	date = forms.DateField(label='Date', required=True, widget=forms.DateInput(attrs={'type' : 'date'}))
	hour = forms.TimeField(label='Hour', required=True, widget=forms.TimeInput(attrs={'type' : 'time'}))

from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import ActivitySchedule
from .forms import Message_Form

response = {'author' : 'Hana Dior'}
def index(request):
    return render(request,'index.html')
def contact(request):
    return render(request,'index.html')
def about(request):
    return render(request,'about.html')
def kitchen(request):
    return render(request,'kitchen.html')
def signup(request):
    return render(request,'signup.html')
def schedule(request):
	form = Message_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['activity'] = request.POST['activity']
		response['category'] = request.POST['category']
		response['place'] = request.POST['place']
		response['day'] = request.POST['day']
		response['date'] = request.POST['date']
		response['hour'] = request.POST['hour']
		message = ActivitySchedule(activity=response['activity'], category=response['category'], place=response['place'], day=response['day'], date=response['date'], hour=response['hour'])
		message.save()
	else:
		print('The input is wrong')
	response['form_message'] = form
	html = 'fillform.html'
	return render(request, html, response)
def schedule_result(request):
	results = ActivitySchedule.objects.all()
	response['form_result'] = results
	return render(request, 'resultform.html', response)
def schedule_delete(request):
	ActivitySchedule.objects.all().delete()
	return HttpResponseRedirect("../schedule_result")
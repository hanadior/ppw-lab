from django.conf.urls import include
from django.urls import re_path
from django.contrib import admin
from lab_1.views import index as index_lab1
import MyApp.urls as MyApp

urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^lab-1/', include('lab_1.urls')),
    re_path(r'^lab-2/', include('lab_2.urls')),
	re_path(r'^MyApp/', include(('MyApp.urls', 'MyApp'), namespace='MyApp')),
]
